package angajatiapp.model;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class EmployeeTest {
    private Employee e1, e2, e3;

    @BeforeEach
    public void Setup() {
        e1 = new Employee();
        e2 = new Employee();
        e3 = null;
        e1.setFirstName("John");
        e2.setFirstName("");
        e1.setCnp("1234567890123");
    }

    @AfterEach
    public void Reset() {
        e1 = null;
        e2 = null;
        e3 = null;
    }

    @Test
    @Order(1)
    void getFirstName() {
        assertEquals("John", e1.getFirstName());
        assertEquals(4, e1.getFirstName().length());

        assertEquals("", e2.getFirstName());
        assertEquals(0, e2.getFirstName().length());
    }

    @Test
    @Order(2)
    void getFirstNameNull() {
//        try {
//            assertEquals("", e3.getFirstName());
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//        }

        assertThrows(Exception.class, () -> e3.getFirstName());
    }

    @Disabled
    @Test
    @Order(4)
    void getFirstNameDisabled() {
//        Test should fail but it is disabled
        assertEquals("TestName", e1.getFirstName());
    }

    @Test
    @Order(3)
    void setFirstName() {
        assertEquals("John", e1.getFirstName());
        e1.setFirstName("Jane");
        assertNotEquals("John", e1.getFirstName());
        assertEquals("Jane", e1.getFirstName());
    }

    @Test
    @Order(6)
    void getCnp() {
        assertEquals(13, e1.getCnp().length());
    }

    @Test
    @Order(5)
    void setCnp() {
        assertEquals(13, e1.getCnp().length());

        e1.setCnp("12345");

        try {
            assertEquals(5, e1.getCnp().length());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    @Order(7)
    void constructorEmployee() {
        Employee e4 = new Employee();
        assertNotEquals(e1.getCnp(), e4.getCnp());

        assertEquals("", e4.getCnp());

        e4.setCnp("1234567890123");
        assertEquals(e1.getCnp(), e4.getCnp());
    }

    @ParameterizedTest
    @ValueSource(doubles = {100, 0, -9999, 2131, 1})
    @Order(8)
    void salaryParamTests(double salary) {
        e1.setSalary(salary);
        assertEquals(salary, e1.getSalary());
    }

    @Test
    @Timeout(value = 100, unit = TimeUnit.MILLISECONDS)
    void checkFirstNameTimeout() {
        try {
            TimeUnit.MILLISECONDS.sleep(20);
            assertEquals("John",e1.getFirstName());
        } catch (InterruptedException ie) {
            System.out.println(ie.getMessage());
        }
    }
}