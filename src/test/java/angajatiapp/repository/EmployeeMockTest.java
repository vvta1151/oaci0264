package angajatiapp.repository;

import angajatiapp.controller.DidacticFunction;
import angajatiapp.controller.EmployeeController;
import angajatiapp.model.Employee;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class EmployeeMockTest {
    private EmployeeController employeeController;
    private EmployeeMock employeeMock;
    private Employee emp;
    int numberOfEmployees;


    @BeforeEach
    public void Setup() {
        employeeMock = new EmployeeMock();
        employeeController = new EmployeeController(employeeMock);
        numberOfEmployees = employeeController.getEmployeesList().size();

        emp = new Employee("Mary", "Jane", "1234567890123", DidacticFunction.CONFERENTIAR, 5000d);
    }

    @AfterEach
    public void Reset() {
        emp = null;
    }

    @Test
    void addEmployeeTC1() {
        employeeController.addEmployee(emp);
        assertEquals(employeeController.getEmployeesList().size(), numberOfEmployees+1, "Employee added successfully");
//        System.out.println(employeeController.getEmployeesList());
    }

    @Test
    void addEmployeeTC2() {
        emp.setLastName("Joe");
        employeeController.addEmployee(emp);
        assertEquals(employeeController.getEmployeesList().size(), numberOfEmployees+1, "Employee added successfully");
//        System.out.println(employeeMock.getEmployeeList());
    }

    @Test
    void addEmployeeTC3() {
        emp.setFirstName(null);

        assertThrows(Exception.class, () -> employeeController.addEmployee(emp));
    }

    @Test
    void addEmployeeTC4() {
        emp.setFirstName("");
        System.out.println(emp);
        assertEquals(employeeController.getEmployeesList().size(), numberOfEmployees, "Invalid First Name. Employee NOT added");
/*
        try {
            employeeController.addEmployee(emp);
            assert(true);
        } catch (Exception e) {
            assert(true);
        }
*/

    }

    @Test
    void addEmployeeTC5() {
        emp.setCnp("123");

        assertEquals(employeeController.getEmployeesList().size(), numberOfEmployees, "Invalid CNP. Employee NOT added");
/*
        try {
            employeeController.addEmployee(emp);
            assert(true);
        } catch (Exception e) {
            assert(true);
        }
*/
    }

    @Test
    void addEmployeeTC6() {
        emp.setCnp("9999999999999999");

        assertEquals(employeeController.getEmployeesList().size(), numberOfEmployees, "Invalid CNP. Employee NOT added");

/*
        try {
            employeeController.addEmployee(emp);
            assert(true);
        } catch (Exception e) {
            assert(true);
        }
*/
    }

    @Test
    void addEmployeeTC7() {
        emp.setSalary(0d);

        assertEquals(employeeController.getEmployeesList().size(), numberOfEmployees, "Invalid Salary. Employee NOT added");

/*
        try {
            employeeController.addEmployee(emp);
            assert(true);
        } catch (Exception e) {
            assert(true);
        }
*/
    }

    @Test
    void addEmployeeTC8() {
        emp.setSalary(-100d);

        assertEquals(employeeController.getEmployeesList().size(), numberOfEmployees, "Invalid Salary. Employee NOT added");

/*
        try {
            employeeController.addEmployee(emp);
            assert(true);
        } catch (Exception e) {
            assert(true);
        }
*/
    }
}